'use strict';

/**
 * @ngdoc function
 * @name clientApp.controller:MainController
 * @description
 * # MainController
 * Controller of the clientApp
 */
angular.module('app')
  .controller('MainController', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
