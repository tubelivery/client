'use strict';

/**
 * @ngdoc function
 * @name clientApp.controller:AboutController
 * @description
 * # AboutController
 * Controller of the clientApp
 */
angular.module('app')
  .controller('AboutController', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
