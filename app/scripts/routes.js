app.config(function($stateProvider, $urlRouterProvider) {
  $urlRouterProvider.otherwise('/');
  $stateProvider
    .state('home',{
      url : '/',
      views: {
        'main': {
          templateUrl : 'views/home.html',
          controller: 'MainController'
        }
      }
    })
    .state('about', {
      url: '/about',
      views: {
        'main': {
          templateUrl: 'views/about.html',
          controller: 'AboutController'
        }
      }
    });
  }
);
